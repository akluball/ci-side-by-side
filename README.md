# ci-side-by-side

A collection of `sh` scripts for setting up local ci infrastructure.
See [examples] for small projects using the various setups.
See [config] for configuration variable reference.

## Installation

Clone the repository, move into it, and install symlink to sbs:
```
git clone https://gitlab.com/admklu/ci-side-by-side.git
cd ci-side-by-side
./install [--to DIR] [--as LINK-NAME]
```
The default for `DIR` is /usr/local/bin.
The default for `LINK-NAME` is sbs.

Uninstall symlink to sbs:
```
./uninstall
```

## Network

Creating the network is required before bringing up any other infrastructure.

Create network:
```
sbs network up
```

Delete network:
```
sbs network down
```

## Concourse

Setup concourse:
```
sbs concourse up [--user CONCOURSE-USER] [--pass CONCOURSE-PASS] [--login TARGET]
```
The default for `CONCOURSE_USER` is the output of `whoami`.
The default for `CONCOURSE_PASS` is simply pass.
If `--login` is passed, then the fly `TARGET` login will automatically be created.

Teardown concourse:
```
sbs concourse down
```

## Buildkit

Setup buildkit:
```
sbs buildkit up
```

Teardown buildkit:
```
sbs buildkit down
```

## Registry

Setup (docker) registry:
```
sbs registry up
```
Concourse should be setup prior to the registry.
This allows the worker to be configured to trust the registry.

Teardown registry:
```
sbs registry down
```

## Environment Variables

Export sbs environment variables:
```
$(sbs env exports)
```
Current exports include concourse worker container name (SBS_CONCOURSE_WORKER), registry container name
(SBS_REGISTRY), and buildkit address (SBS_BUILDKIT_ADDR).

[examples]: examples
[config]: config
