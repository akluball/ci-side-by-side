# Build Run

This example project requires concourse, buildkit, and registry.

## Setup

Set `TARGET` to the concourse `<target>` you are logged in to:
```
TARGET=<target> # (replaced with yours)
```

Set pipeline:
```
fly set-pipeline \
    --target "$TARGET" \
    --config pipeline.yaml \
    --var buildkit-addr=tcp://ci-buildkit.test:1234 \
    --var registry=ci-registry.test \
    --pipeline build-push-run
```

Unpause pipeline:
```
fly unpause-pipeline \
    --target "$TARGET" \
    --pipeline build-push-run
```

## Verification

Run build-and-push job and watch logs:
```
fly trigger-job \
    --target "$TARGET" \
    --job build-push-run/build-and-push \
    --watch
```

Run run job and watch logs:
```
fly trigger-job \
    --target "$TARGET" \
    --job build-push-run/run \
    --watch
```

## Teardown

Delete pipeline:
```
fly destroy-pipeline \
    --target "$TARGET" \
    --pipeline build-push-run
```