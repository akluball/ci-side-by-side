# Hello World

This example project requires concourse.

## Setup

Set `TARGET` to the concourse `<target>` you are logged in to:
```
TARGET=<target> # (replaced with yours)
```

Set pipeline:
```
fly set-pipeline \
    --target "$TARGET" \
    --config pipeline.yaml \
    --pipeline hello-world
```

Unpause pipeline:
```
fly unpause-pipeline \
    --target "$TARGET" \
    --pipeline hello-world
```

## Verification

Run hello-world job and watch logs:
```
fly trigger-job \
    --target "$TARGET" \
    --job hello-world/run \
    --watch
```

## Teardown

Delete pipeline:
```
fly destroy-pipeline \
    --target "$TARGET" \
    --pipeline hello-world
```