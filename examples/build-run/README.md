# Build Run

This example project requires concourse and buildkit.

## Setup

Set `TARGET` to the concourse `<target>` you are logged in to:
```
TARGET=<target> # (replaced with yours)
```

Set pipeline:
```
fly set-pipeline \
    --target "$TARGET" \
    --config pipeline.yaml \
    --var buildkit-addr=tcp://ci-buildkit.test:1234 \
    --pipeline build-run
```

Unpause pipeline:
```
fly unpause-pipeline \
    --target "$TARGET" \
    --pipeline build-run
```

## Verification

Run build-run job and watch logs:
```
fly trigger-job \
    --target "$TARGET" \
    --job build-run/build-run \
    --watch
```

## Teardown

Delete pipeline:
```
fly destroy-pipeline \
    --target "$TARGET" \
    --pipeline build-run
```